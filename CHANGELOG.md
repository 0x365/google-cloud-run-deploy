# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.4

- minor: Update cloud SDK and change from dockerhub to gcr.io
- minor: Support multiples lines in ENVIRONMENT section
- minor: Added DEBUG validations

## 0.1.3

- minor: Support Cloud SQL, update sdk cloud 

## 0.1.2

- minor: Add map to custom domain.

## 0.1.1

- minor: Documentation added and variables support added.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines Google Cloud Run deploy pipe.

