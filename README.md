# Bitbucket Pipelines Pipe: Google Cloud Run Deploy

Pipe to deploy to [Google Cloud Run][gcloud-run-deploy].
Deploy docker image into a Google Cloud Run. Create the service if is not created.

This pipe is based on [google-cloud-storage-deploy](https://bitbucket.org/atlassian/google-cloud-storage-deploy/src/master/) pipe, this is not an atlassian official pipe, but at this moment not exist a pipeline to deploy to Cloud Run.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: 0x365/google-cloud-run-deploy:0.1.3
  variables:
    KEY_FILE: '<string>'
    PROJECT: '<string>'
    SERVICE: '<string>'
    IMAGE: '<string>'
    # ENVIRONMENT: '<string>' # Optional.
    # CLOUDSQL_INSTANCE: '<string>' # Optional.
    # REGION: '<string>' # Optional.
    # MEMORY: '<integer>' # Optional.
    # CPU: '<integer>' # Optional.
    # PORT: '<integer>' # Optional.
    # PUBLIC: '<string>' # Optional.
    # TIMEOUT: '<integer>' # Optional.
    # DOMAIN: '<string>' # Optional.
```

## Variables

| Variable                   | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |
| KEY_FILE (*)                  |  base64 encoded Key file for a [Google service account](https://cloud.google.com/iam/docs/creating-managing-service-account-keys). |
| PROJECT (*)                   |  The project that owns the cloud run service to deploy into. |
| SERVICE (*)                    |  Google Cloud Run service  name. |
| IMAGE (*)                    |  The source of the docker image to deploy |
| ENVIRONMENT                    |  Define environment variables. format: `VAR1=VALUE1,VAR2=VALUE2,VAR3=VALUE3` |
| CLOUDSQL_INSTANCE             | Set Cloud SQL instances with the given values. format: `CONNECTION_NAME1,CONNECTION_NAME2` |
| REGION                           |  Region where will run the container. Valid values are: `us-central1`, `us-east1`, `us-east4`, Default `us-central1`. |
| MEMORY                    |  How much memory is assigned to the task. Default: `128` |
| CPU                    |  How many processors assign to this task. Default: `1` |
| PORT                         |  Which port listen the container. Default: `false`. |
| PUBLIC                         |  Allow external connections. Default: `true`. |
| TIMEOUT                         |  How much time before timeout. Default: `300`. |
| DOMAIN                         |  Add mapping to a custom domain. [_Domain need to be verified_](https://cloud.google.com/run/docs/mapping-custom-domains?_ga=2.163902712.-677121002.1579570359#command-line)  |


_(*) = required variable._

More info about parameters and values can be found in the [Google Cloud Run deploy docs][gcloud-run-deploy].

## Examples

### Basic example:

```yaml
script:
  - pipe: 0x365/google-cloud-run-deploy:0.1.3
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'my-project'
      SERVICE: 'demo-0x365'
      IMAGE: 'us.gcr.io/eidast-personal-labs/demos/demo-0x365:latest'
      ENVIRONMENT: 'VAR1=VALUE1,VAR2=VALUE2'
```

### Advanced example: 
    
```yaml
script:
  - pipe: 0x365/google-cloud-run-deploy:0.1.3
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'my-project'
      SERVICE: 'demo-0x365'
      IMAGE: 'us.gcr.io/eidast-personal-labs/demos/demo-0x365:latest'
      PUBLIC: 'true'
      TIMEOUT: '10'
      ENVIRONMENT: 'VAR1=VALUE1,VAR2=VALUE2'
      CLOUDSQL_INSTANCE: 'PROJECT:REGIONS:INSTANCE_NAME'
      DOMAIN: "demo-0x365.domain.com"
```

## TODO

- Secret Management

## Support
If you’d like help with this pipe, or you have an issue or feature request, send me an email to alexander.moreno at 0x365.com or 

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2020 Alexander Moreno Delgado and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,google
[gcloud-run-deploy]: https://cloud.google.com/sdk/gcloud/reference/run/deploy
