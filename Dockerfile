FROM gcr.io/google.com/cloudsdktool/cloud-sdk:324.0.0-alpine

RUN gcloud components install beta --quiet
COPY pipe /usr/bin/
RUN chmod +x /usr/bin/pipe.sh

ENTRYPOINT ["/usr/bin/pipe.sh"]