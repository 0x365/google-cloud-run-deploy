#!/usr/bin/env sh

# Deploy to Google Cloud Run, https://cloud.google.com/run/
#
# Required globals:
#   KEY_FILE
#   PROJECT
#   SERVICE
#   IMAGE
#
# Optional globals:
#   EXTRA_ARGS
#   REGION
#   MEMORY
#   CPU
#   PORT
#   DOMAIN
#   CLOUDSQL_INSTANCE

source "$(dirname "$0")/common.sh"
enable_debug

# mandatory parameters
KEY_FILE=${KEY_FILE:?'KEY_FILE variable missing.'}
PROJECT=${PROJECT:?'PROJECT variable missing.'}
SERVICE=${SERVICE:?'SERVICE variable missing.'}
IMAGE=${IMAGE:?'IMAGE variable missing.'}

# Optional parameters
REGION=${REGION:-'us-central1'}
MEMORY=${MEMORY:-'128'}
CPU=${CPU:-'1'}
PORT=${PORT:-'8080'}
PUBLIC=${PUBLIC:-'true'}
TIMEOUT=${TIMEOUT:-'300'}
ENVIRONMENT=${ENVIRONMENT:-''}
CLOUDSQL_INSTANCE=${CLOUDSQL_INSTANCE:-''}
DOMAIN=${DOMAIN:-''}
DEBUG=${DEBUG:-'false'}

info "Validating variables"

SQL=""
VARIABLE=""

if [[ $CLOUDSQL_INSTANCE != "" ]]; then
  SQL="--set-cloudsql-instances=${CLOUDSQL_INSTANCE}"
fi

if [[ $PUBLIC == 'true' ]]; then 
  CONNECTION='--allow-unauthenticated'
else
  CONNECTION='--no-allow-unauthenticated'
fi

info "Validating inputs variables"

CACHE_IFS=$IFS
export IFS=$'\n'

if [ "$ENVIRONMENT" != "" ] 
  then
    ENV=""
    VARIABLES=`echo $ENVIRONMENT | tr "," "\n"`
    for variable in $VARIABLES
      do
        # Trim $variable
        TMP_variable=`echo $variable | sed -e 's/^[[:space:]]*//'`
        ENV="${ENV} --set-env-vars \"${TMP_variable}\""
      done
  else
    ENV=""
fi

export IFS=$CACHE_IFS

if [ ${DEBUG} == "true" ]
  then
    echo ${ENV}
fi

info "Setting up environment".
if [ ${DEBUG} == "true" ]
  then
    echo "${KEY_FILE}" | base64 -d >> /tmp/key-file.json
fi

run gcloud auth activate-service-account --key-file /tmp/key-file.json --quiet ${gcloud_debug_args}
run gcloud config set project $PROJECT --quiet ${gcloud_debug_args}

info "Starting deployment"

run gcloud run deploy ${SERVICE} --image=${IMAGE} ${SQL} --region=${REGION} --cpu=${CPU} --memory=${MEMORY} --platform=managed --port=${PORT} --timeout=${TIMEOUT} --project=${PROJECT} ${CONNECTION} ${ENV}

if [ "$DOMAIN" != "" ]; then
    info "Add map to custom domain"
    if [ "$DEBUG" == "true" ]; then
      gcloud domains list-user-verified
    fi
    
    if $(gcloud beta run domain-mappings list --project=${PROJECT} --platform=managed --region=${REGION} | grep -q ${DOMAIN}); then
      info "Custom domain map already exist - SKIPPING"
    else
      run gcloud beta run domain-mappings create --service ${SERVICE} --domain ${DOMAIN} --region=${REGION} --platform=managed --force-override --quiet
    fi
fi

if [ "${status}" -eq 0 ]; then
  success "Deployment successful."
else
  fail "Deployment failed."
fi